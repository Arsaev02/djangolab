from rest_framework import serializers
from core.serializers import ImageSerializer, UserSerializer

from .models import Driver, VehicleBrand, VehicleType, VehicleModel, Vehicle, VehicleImage, VehicleStatus


class DriverSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Driver
        fields = '__all__'


class VehicleBrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleBrand
        fields = '__all__'


class VehicleTypeSerializer(serializers.ModelSerializer):
    image = ImageSerializer()

    class Meta:
        model = VehicleType
        fields = '__all__'


class VehicleModelSerializer(serializers.ModelSerializer):
    type = VehicleTypeSerializer()

    class Meta:
        model = VehicleModel
        fields = '__all__'


class VehicleSerializer(serializers.ModelSerializer):
    driver = DriverSerializer()
    model = VehicleModelSerializer()

    class Meta:
        model = Vehicle
        fields = '__all__'


class VehicleImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleImage
        fields = '__all__'


class VehicleStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleStatus
        fields = '__all__'
