import React, { useEffect, useState } from 'react';
import { IonButton, IonCol, IonRow, IonContent, IonModal, IonItem, IonInput, IonText, IonDatetime } from '@ionic/react';
import './NewOrder.css';
import OrderReady from './OrderReady';
import { useHistory } from 'react-router';
import axios from 'axios';
import { ApiUrl } from "../services/ApiService";
import VehicleTypeSelector from "./VehicleTypeSelector";
import City from '../models/Category';
import Address from '../models/Address';

export interface MapAddress {
  coords: number[],
  name?: string,
  city?: string
}

interface NewOrder {
  idCategory: string;
  addresses: MapAddress[];
}

 const NewOrder:  React.FC<NewOrder> = ({ idCategory, addresses }) => {
  const [showModal, setShowModal] = useState(false);

  const apiUrlCategory = ApiUrl + "category-vehicle-type/";
  const apiUrlJobs = ApiUrl + "jobs/";
  const apiUrlAddress = ApiUrl + "address/";
  const apiUrlCity = ApiUrl + "city/";

  const [categoryVehicleTypes, setCategoryVehicleTypes] = useState([]);
  const [city, setCity] = useState<City[]>([]);


  useEffect(() => {
    fetch(apiUrlCity).then(async response => {
      setCity(await response.json());
    });
  }, []);

  useEffect(() => {
    fetch(apiUrlCategory).then(async response => {
      setCategoryVehicleTypes(await response.json());
    });
  }, []);

  const [vehicleId, setVehicleId] = useState<number>(0);
  const [commentDriver, setCommentDriver] = useState<string>();
  const [volume, setVolume] = useState<string>();
  const [weight, setWeight] = useState<string>();
  const [dateTime, setdateTime] = useState<string>();
  const [modalReady, setModalReady] = useState(false);

  let history = useHistory();

  return (
    <div className="container-place-order">
      <div>
        <IonRow>
          <IonCol size="1">
            <div className="position-div div-purple"> </div>
          </IonCol>
          <IonCol className="black-text" size="11">
            {addresses.length > 0 ? addresses[0].name : null}
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol size="1">
            <div className="position-div div-yellow"> </div>
          </IonCol>
          <IonCol className="black-text" size="11">
            {addresses.length > 1 ? addresses[1].name : null}
          </IonCol>
        </IonRow>
        <VehicleTypeSelector
          onTypeClicked={id => setVehicleId(id)}
          activeId={vehicleId}
          vehicleTypes={categoryVehicleTypes.filter((vtype: any) => vtype.category.id == idCategory)} />
        <IonRow>
          <IonCol size="6">
            <IonRow>
              <div className="black-text">
                <img className="payment-image" src="../../assets/icon/visa_PNG3.png" />
              .1234
            </div>
            </IonRow>
          </IonCol>
          <IonCol size="6">
            <div className="wishes black-text">
              <img src="../../assets/icon/plus.png" className="icon" />
              Пожелания
            </div>
          </IonCol>
        </IonRow>
        <section>

          <IonContent>
            <IonModal isOpen={showModal} cssClass='my-custom-class'>
              <IonText className="wish-margin black-text"> <strong>Пожелания {vehicleId}</strong></IonText>
              <IonText className="wish-margin black-text"> Укажите ваши пожелания и дополнительную информацию, если считаете её необходимой</IonText>
              <IonItem>
                <IonInput value={commentDriver} onIonChange={(e) => setCommentDriver((e.target as HTMLInputElement).value)} placeholder="Комментарии водителю" />
              </IonItem>
              <IonItem>
                <IonInput value={volume} onIonChange={(e) => setVolume((e.target as HTMLInputElement).value)} placeholder="Объем м&sup3;" />
              </IonItem>
              <IonItem>
                <IonInput value={weight} onIonChange={(e) => setWeight((e.target as HTMLInputElement).value)} placeholder="Вес кг" />
              </IonItem>
              <IonItem>
                <IonDatetime value={dateTime} onIonChange={(e) => setdateTime((e.target as HTMLInputElement).value)} placeholder="Время и дата" display-timezone="utc" />
              </IonItem>

              <IonButton color="primary" expand="block" className="place-button" onClick={async () => {

                //проверяет есть ли такой город уже в списке если нет то добавляет
                var cityId1;
                var cit=city.find(x=>x.name==addresses[0].city)
                if(cit){
                  cityId1=cit.id;
                }else{
                  await axios.post(apiUrlCity, {
                    name: addresses[0].city
                  })
                }
                var cityId2;
                var cit=city.find(x=>x.name==addresses[0].city)
                if(cit){
                  cityId2=cit.id;
                }else{
                  await axios.post(apiUrlCity, {
                    name: addresses[1].city
                  })
                }
                //если одного из городов нет, то берет новые данные с апи
                if(cityId1 && cityId2){
                  var newDataCity:City[];
                  await axios.get(apiUrlCity, {
                  })
                  .then(function (response) {
                    newDataCity=response.data;
                    cityId1=newDataCity.find(x=>x.name==addresses[0].city)?.id;
                    cityId2=newDataCity.find(x=>x.name==addresses[1].city)?.id;
                  })
                  .catch(function (error) {
                    console.log(error);
                  })
                  .then(function () {
                  });  
                }
              
                console.log("pered post");

                console.log(addresses[0].name);
                console.log(addresses[0].coords[0] + " " + addresses[0].coords[1]);
                console.log(addresses[0].name?.split(',')[1]);
                console.log(addresses[0].name?.split(',')[0]);
                console.log(cityId1);
                
                console.log("pered post2");

                console.log(addresses[1].name);
                console.log(addresses[1].coords[0] + " " + addresses[0].coords[1]);
                console.log(addresses[1].name?.split(',')[1]);
                console.log(addresses[1].name?.split(',')[0]);
                console.log(cityId2);

                axios.post(apiUrlAddress, {
                  full_address: addresses[0].name,
                  location: addresses[0].coords[0] + " " + addresses[0].coords[1],
                  house: addresses[0].name?.split(',')[1],
                  street: addresses[0].name?.split(',')[0],
                  city: cityId1
                })
                  .then(function (res: any) {
                    console.log("res " + res);
                  })
                  .catch(function (error: any) {
                    console.log(error);
                  });

                axios.post(apiUrlAddress, {
                  full_address: addresses[1].name,
                  location: addresses[1].coords,
                  house: addresses[1].name?.split(',')[1],
                  street: addresses[1].name?.split(',')[0],
                  city: cityId2
                })
                  .then(function (res: any) {
                    console.log("res " + res);
                  })
                  .catch(function (error: any) {
                    console.log(error);
                  });
                  

                  var Addre:Address[];
                  var addr;
                  var drop;
                  await axios.get(apiUrlAddress, {
                  })
                  .then(function (response) {
                    Addre=response.data;
                    addr=Addre.find(x=>x.location == (addresses[0].coords[0] + " " + addresses[0].coords[1]));
                    drop=Addre.find(x=>x.location == (addresses[1].coords[0] + " " + addresses[1].coords[1]));
                  }) 
                  console.log(addr);
                  console.log(drop);


                console.log("req body", vehicleId, commentDriver, volume, weight, dateTime);
                axios.post(apiUrlJobs, {
                  name: commentDriver,//todo нужно поле
                  category: idCategory,
                  description: commentDriver,
                  address: addr ,
                  dropoff: drop,
                  quantity: volume,
                  date: dateTime,
                  vehicle_type: vehicleId,
                  status: 1,
                  customer: 1//todo need add id user
                  //weight свободно
                })
                  .then(function (res: any) {
                    console.log("res " + res);
                  })
                  .catch(function (error: any) {
                    console.log(error);
                  });
                setShowModal(false);
                setModalReady(true);
              }}>Готово</IonButton>


              <IonContent>
                <IonModal isOpen={modalReady} cssClass='my-custom-class'>
                  <OrderReady />
                  <IonButton color="primary" expand="block" className="place-button" onClick={() => {
                    history.goBack()
                  }
                  }>Вернуться в меню заказов</IonButton>
                </IonModal>
              </IonContent>

            </IonModal>
          </IonContent>
          <IonButton onClick={() => {
            if (!vehicleId) {
              console.log("Не выбран тип машины (ид)");
              return;
            } else {
              setShowModal(true)
            }
          }
          } color="primary" expand="block" className="place-button">Заказать</IonButton>
        </section>
      </div>
    </div>
  );
};

export default NewOrder;
