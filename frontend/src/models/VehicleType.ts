import Image from "./Image";

export default class VehicleType {
  constructor(
    public id: number,
    public name: string,
    public description: string,
    public image: Image) { }
}
